let collection = [];

// Write the queue functions below.
// const print = () => {
// 	return collection
// }

function print(){
	return collection
}

function enqueue(element){
	collection.push(element)
	return collection
}

function dequeue(){
	collection.shift()
	return collection
}

function front(){
	return collection[0]
}

function isEmpty(){
	if(collection.length == 0){
		return true
	} else {
		return false
	}
}

function size(){
	return collection.length
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};